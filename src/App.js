import './App.css';
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import ProductView from "./components/ProductView"
import UserProfile from "./components/UserProfile"
import Home from "./pages/Home";
import PaymentPage from "./pages/PaymentPage";
import Users from "./pages/Users";
import Products from "./pages/Products";
import { UserProvider } from './UserContext';
import AppNavbar from "./pages/AppNavbar";
import { useState, useEffect } from 'react';
import { BrowserRouter as Router,Routes, Route, Navigate } from 'react-router-dom';
import { CartProvider, useCart } from "react-use-cart";

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const {emptyCart} = useCart();

  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  },[user])

  if(user.id === null) {
    emptyCart();
  }

  return (
    <CartProvider>
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Register />} />
          <Route path='/products' element={<Products />} />
          <Route path='/products/:productId' element={<ProductView />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/payment' element={<PaymentPage />} />
          <Route 
              path='/ViewUsers' 
              element={
                (user.isAdmin===true)? (
                  <Users />
                  ) : (
                    <Navigate to = "/login"/>
                  )
                } 
              />
          <Route 
              path='/UserProfile' 
              element={
                (user.id !== null)? (
                  <UserProfile />
                  ) : (
                    <Navigate to = "/login"/>
                  )
                } 
              />
        </Routes>
        <AppNavbar />
      </Router>
    </UserProvider>
    </CartProvider>
  );
}

export default App;

