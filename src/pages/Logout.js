import { Navigate } from 'react-router-dom';
import { useEffect, useContext } from 'react';
import UserContext from "../UserContext";
import Swal from 'sweetalert2';
import {useCart} from 'react-use-cart'


export default function Logout() {
	const {emptyCart} = useCart();
	const { unsetUser, setUser } = useContext(UserContext);
	unsetUser();
	useEffect(()=>{
	setUser({id:null})
	emptyCart();
	Swal.fire({
		title: "Adios",
		icon: "success",
		text: "Successfully logout"
	})
	},[])
	return(
		<Navigate to='/' />
	)
}
