import {useState, useEffect} from 'react';
import {Form, Button, FloatingLabel, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router";
import Swal from 'sweetalert2';

export default function Register () {
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [form, setForm] = useState({
	  firstname: "",
	  lastname: "",
	  email: "",
	  mobilenumber: "",
	  password: ""
	});
	const navigate = useNavigate();

	function updateForm(value) {
	   return setForm((prev) => {
	   	 setPassword2('');
	     return { ...prev, ...value };
	   });
	 }

	async function onSubmit (e){
		e.preventDefault();
			if(form.mobilenumber.length < 11){
				Swal.fire({
					title: "Minimum length for mobile number must be 11 digits or greater.",
					icon: "error",
					text: "Input your full mobile number"
				})
			}
			else{
				const newPerson = { ...form };
				await fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify(newPerson),
				})
				.catch(error=>{
					window.alert(error);
		     		return;
				}).then(res => res.json()).then(data => {
						if(data === false){
							Swal.fire({
								title: "Email already exists!",
								icon: "error",
								text: "Please try another email"
							})
						}
						else if (data === true){
							Swal.fire({
								title: "Registration successful!",
								icon: "success",
								text: "Welcome to AviloPC!"
							})
							navigate("/login");
						}
						else{
							Swal.fire({
								title: "Error 404",
								icon: "error",
								text: "There seems to be a problem with the connection, please try again."
							})
						}
					})
					setForm({
					firstname: "",
					lastname: "",
					email: "",
					mobilenumber: "",
					password: ""
			});
		setPassword2('');
		}
	}

	useEffect (()=>{
		if(password2 === ""){
			return;
		}
		else if(form.password === password2){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[form,password2]);

	return (
		<Container fluid className = "reg-form">
			<div>
			<h2 className = "py-4 text-center"> ACCOUNT REGISTRATION </h2>
			</div>
	    <Form onSubmit = {onSubmit}>
	      <FloatingLabel controlId="firstname" label="First Name" className="mb-3">
        <Form.Control 
		    type="text" 
		    placeholder="Enter your first name"
		    maxLength = "30" 
		    value = {form.firstname} 
		    onChange = {(e)=>updateForm({firstname: e.target.value})}	
		    required/>
	      </FloatingLabel>

        <FloatingLabel controlId="lastname" label="Last Name" className="mb-3">
        <Form.Control 
		    type="text" 
		    placeholder="Enter your last name"
		    maxLength = "30" 
		    value = {form.lastname} 
		    onChange = {(e)=>updateForm({lastname: e.target.value})}	
		    required/>
		    </FloatingLabel>

	      <FloatingLabel controlId="email" label="Email" className="mb-3">
        <Form.Control 
		    type="email" 
		    placeholder="Enter your email"
		    maxLength = "40" 
		    value = {form.email} 
		    onChange = {(e)=>updateForm({email: e.target.value})}	
		    required/>
	      </FloatingLabel>

        <FloatingLabel controlId="mobilenumber" label="Mobile no." className="mb-3">
        <Form.Control 
        	type="number"
        	placeholder="Mobile number"
        	value = {form.mobilenumber} 
		    	onChange = {(e)=>updateForm({mobilenumber: e.target.value})} 
        	required/>
        </FloatingLabel>

	      <FloatingLabel controlId="password" label="Password" className="mb-3">
        <Form.Control 
        	type="password" 
        	placeholder="Password"
        	maxLength = "30" 
        	value = {form.password} 
		    	onChange = {(e)=>updateForm({password: e.target.value})}
        	required/>
	      </FloatingLabel>

        <FloatingLabel controlId="password2" label="Confirm Password" className="mb-3">
        <Form.Control 
        	type="password" 
        	placeholder="Password"
        	maxLength = "30"
        	value = {password2} 
		    	onChange = {(e)=>setPassword2(e.target.value)} 
        	required/>
        </FloatingLabel>
        {isActive ?
        <div className = "text-center">
		      <Button className= "register-button mt-3" variant="primary" type="submit" id="submitBtn">
		        Register
		      </Button>
	      </div>
	      :
	      <div className = "text-center">
		      <Button className= "register-button mt-3" variant="primary" type="submit" id="submitBtn" disabled>
		        Register
		      </Button>
	      </div>
	  		}
	    </Form>
	    <div className = "my-3 container-fluid d-flex justify-content-center">
	    	<div>
	    		<p> Already have an account? </p>
	    	</div>
	    	<div>
	    		<Link size = "sm" to = "/login"> Login here.</Link>
	    	</div>
	    </div>
	  </Container>
	);
}