import { useContext, Fragment, useState, useEffect } from 'react';
import { Nav, Navbar, Container, Modal, Form, Row, Col, Table, Button, Offcanvas, Card, Image } from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCartShopping} from '@fortawesome/free-solid-svg-icons';
import { CartProvider, useCart } from "react-use-cart";
import Swal from 'sweetalert2';


export default function AppNavbar(props){

	const {products} = props;
	const {user} = useContext(UserContext);
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	const [showCart, setShowCart] = useState(false);
	const closeCart = () => setShowCart(false);
	const [cart, setCart] = useState('');
	const {
		addItem,
	    isEmpty,
	    totalUniqueItems,
	    items,
	    updateItemQuantity,
	    removeItem,
	    cartTotal,
	    emptyCart
	  } = useCart();

	useEffect(()=>{

	const cartArr = items.map(newCart => {
		return(
			<tr key = {newCart.id}>
				<td>{newCart.name}</td>
				<td><span>&#8369;</span>{newCart.price}</td>
				<td className = "text-center">{newCart.quantity}</td>
				<td className = "d-flex">
					<Button variant = "danger" onClick = {() => updateItemQuantity(newCart.id, newCart.quantity - 1)}>-</Button>
					<Button variant = "dark" onClick = {() => updateItemQuantity(newCart.id, newCart.quantity + 1)}>+</Button>
				</td>
			</tr>
		)
	})
		setCart(cartArr);
	},[showCart, emptyCart])

	return(
		<>
			<Navbar collapseOnSelect fixed = "top" bg="dark" expand="lg" variant = "dark" className = "px-3">
			<Navbar.Brand as={ Link } to='/'>AviloPC</Navbar.Brand>
				<Navbar.Toggle aria-controls="responsive-navbar-nav"/>
				<Navbar.Collapse id="responsive-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link as={ NavLink } to='/' eventKey = "1">Home</Nav.Link>
						<Nav.Link as={ NavLink } to='/products' eventKey = "2"> Products</Nav.Link>
						{user.isAdmin !== true && user.id !== null ?
							<Nav.Link as={ NavLink } to='/UserProfile' eventKey = "8"> Profile </Nav.Link>
							:
							null
						}
						{(user.isAdmin === true && user.id !== null)?
							<Nav.Link as={ NavLink } to='/ViewUsers' eventKey = "3"> Users </Nav.Link>
							:
							null
						}
						{user.id === null ?
						<>
						<Nav.Link className = "d-lg-none" as={ NavLink } to='/login' onClick={handleClose} eventKey = "5"> Login</Nav.Link>
						<Nav.Link className = "d-lg-none" as={ NavLink } to='/register' onClick={handleClose} eventKey = "6"> Register</Nav.Link>
						</>
						:
						<>
						<Nav.Link as={ NavLink } to='/logout' eventKey = "7"> Logout</Nav.Link>
						</>
						}
					</Nav>
					{(user.isAdmin !== true) && (user.id !== null) ? 
						<div className = "cart-container d-flex align-items-center">
						<FontAwesomeIcon className = "cart-icon" icon={faCartShopping} onClick={()=>{
							if (items.length <= 0){
		        				Swal.fire({
		        				title: "Cart is empty",
		        				icon: "error",
		        				text: "You don't have items on your cart, please browse our products to proceed in checkout."
	        				})
        					}else{
				          		setShowCart(true);
				        }}}>
		        		</FontAwesomeIcon>
		        		<Button className = "counter" variant="dark">
				        {totalUniqueItems}
					    </Button>
					    </div>
		        		: null }
				</Navbar.Collapse>
				{(user.id === null)?
				<Button className = "d-lg-block" id = "btn-menu" variant="dark" onClick={handleShow} hidden>
				        Menu
				      </Button>
				      :
				      null
				}
			    <Offcanvas placement = "end" show={show} onHide={handleClose}>
			      <Offcanvas.Header closeButton>
			        <Offcanvas.Title>AviloPC</Offcanvas.Title>
			      </Offcanvas.Header>
			      <Offcanvas.Body>
			      <Row>
				      <Col lg = {12} className = "mb-5">
				      {(user.id !== null)?
				      	null
				      :
				      	<>
				        <Nav.Link className = "my-2" as={ NavLink } to='/login' onClick={handleClose}> Login</Nav.Link>
				        <Nav.Link className = "my-2" as={ NavLink } to='/register' onClick={handleClose}> Register</Nav.Link>
				        </>
				      }
					  </Col>
			      </Row>
			      </Offcanvas.Body>
			    </Offcanvas>
			</Navbar>

			<Modal show={showCart} onHide={closeCart}>
					<Modal.Header closeButton>
						<Modal.Title>Your Cart</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Container fluid>
							<Row>
								<Col>
									<Table striped bordered hover responsive>
										<thead className="bg-dark text-white">
											<tr>
												<th>Name</th>
												<th>Price</th>
												<th>Quantity</th>
												<th></th>
											</tr>					
										</thead>
										<tbody>{cart}</tbody>
									</Table>
									<Table striped bordered hover responsive>
										<thead className="bg-dark text-white">
											<tr>
												<th>Total Price:</th>
											</tr>					
										</thead>
										<tbody className = "d-flex justify-content-center">
										<span>&#8369;</span>{cartTotal}
										</tbody>
									</Table>
								</Col>
							</Row>
						</Container>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick = {()=> emptyCart()}>Clear Cart</Button>
						<Link to = "/payment"><Button variant="success" onClick = {closeCart}>Checkout</Button></Link>
					</Modal.Footer>
			</Modal>
			<div className = "footer bg-dark text-white fixed-bottom d-flex">
					<p className = "footer-text">Gabriell Oliva &copy; 2022</p>
				<div className = "social">
					<a href = "https://www.facebook.com/gabrtielloliva/?viewas=100000686899395"><img src = "../images/facebook.jpg" className = "img-fluid"/></a>
				</div>            
		    </div>

		</>
	)
}