import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from 'sweetalert2';

export default function Login () {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState(''); 
	const [isActive, setIsActive] = useState(false);
	const { user, setUser } = useContext(UserContext);
	
	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(res => res.json()).then(data => {
			setUser({
				id:data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[email,password]);

	function loginUser (e) {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data => {
			console.log(data);
			if(typeof data.access !== 'undefined'){
			localStorage.setItem('token', data.access);
			retrieveUserDetails(data.access);
			Swal.fire({
				title: "Login successful!",
				icon: "success",
				text: "Welcome to AviloPC!!"
			})
			}
			else{
				Swal.fire({
					title: "Authentication failed!",
					icon: "error",
					text: "Check your login credentials and try again."
				})
			}
		});
		setEmail('');
		setPassword('');
		<Navigate to = '/products'/>
	}

	return(
		(user.id !== null)?
		<Navigate to = '/products'/>
		:
		<Container fluid = "lg" id = "login-container">
		<Form onSubmit = {(e)=>loginUser(e)}>
	      <Form.Group controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control
	        size ="sm" 
			    type="email" 
			    placeholder="Enter email" 
			    value = {email} 
			    onChange = {e=>setEmail(e.target.value)}	
			    required/>
	        <Form.Text className="text-muted">
	        </Form.Text>
	      </Form.Group>

	      <Form.Group controlId="password">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password" 
	        	value = {password} 
			    onChange = {e=>setPassword(e.target.value)}
	        	required/>
	      </Form.Group>

	      {isActive ?
	      <Button className= "mt-3" variant="primary" type="submit" id="submitBtn">
	        Login
	      </Button>
	      :
	      <Button className= "mt-3" variant="primary" type="submit" id="submitBtn" disabled>
	        Login
	      </Button>
	  	}
	    </Form>
	    <div id = "login-text">
	    <h5> Don't have an account? </h5>
	    <Link id = "login-link" size = "md" to = "/register"> Register here.</Link>
	    </div>
	  </Container>
  );
}