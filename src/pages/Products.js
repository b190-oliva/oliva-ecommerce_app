import { Fragment, useEffect, useState, useContext } from 'react';
import { Container } from 'react-bootstrap';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext';


export default function Products(){
	const { user } = useContext(UserContext);
	const [ products, setProducts] = useState([]);

	const fetchData = () => {

        fetch(`${ process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
            	setProducts(data);	
        });
    }
		
	useEffect(()=>{
		fetchData();
	},[])

	return(
		<>
            {(user.isAdmin === true)
                ? 
                <AdminView productData={products} fetchData={fetchData}/>
                :
                <Container fluid className = "main-container mt-5"> 
	                <div className = "product-container d-md-flex justify-content-center align-items-center text-center">
	                	<UserView productData={products}/>
	                </div>
	            </Container>
            }
        </>
	)
}

