import { Fragment, useEffect, useState } from 'react';
import ViewUsers from '../components/ViewUsers';


export default function Users () {

	const [allUsers, setAllUsers] = useState([]);
	const [orders, setOrders] = useState([]);

	const fetchUsers = () => {
        fetch(`${ process.env.REACT_APP_API_URL}/users/getAllUsers`,{
        	method: "GET",
        	headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
        })
        .then(res => res.json())
        .then(data => {
            setAllUsers(data);	
        });
    }

	useEffect(()=>{
		fetchUsers();
	},[])

	return(
		<Fragment>    
            <ViewUsers usersData={allUsers} fetchUsers={fetchUsers}/>
        </Fragment>
	)
}