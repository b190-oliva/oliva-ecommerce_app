import {useState, useEffect} from 'react';
import {Container, Button, Modal, Form, Table } from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {useCart} from 'react-use-cart';
import Swal from 'sweetalert2';

export default function CheckOut() {

	const [cart, setCart] = useState('');
	const {items, cartTotal, emptyCart} = useCart();
	const [address, setAddress] = useState('');
	const [paymentMethod, setPaymentMethod] = useState('');
	const [showModal, setShowModal] = useState(false);
	const closeModal = () => setShowModal(false);

	useEffect(()=>{
		
	const cartArr = items.map(newCart => {
		return(
			<tr key = {newCart.id}>
				<td>{newCart.name}</td>
				<td><span>&#8369;</span>{newCart.price}</td>
				<td className = "text-center">x{newCart.quantity}</td>
			</tr>
		)
	})
		setCart(cartArr);
	},[cartTotal, items])

	async function onSubmit (e) {
		e.preventDefault();
		await fetch(`${process.env.REACT_APP_API_URL}/users/orders/checkout`, {
			method: "POST",
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				orders: items,
				shippingAddress: address,
				paymentMethod: paymentMethod
			}),
		})
		.catch(error=>{
			window.alert(error);
     		return;
		})
		.then(res => res.json())
		.then(result => {
			if(result === true){
				Swal.fire({
					title: "Your order was successfully placed!",
					icon: 'success',
					text: 'An email confirmation will be sent for more information about your order'
				})
				closeModal();
				<Navigate to = "/profile"/>
			}
			else{
				Swal.fire({
					title: "Error placing order",
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})

	}

	return(
		<Container fluid className = "checkout">
        	<Table striped bordered hover responsive>
        		<thead className="bg-dark text-white">
        			<tr>
        				<th>Name</th>
        				<th>Price</th>
        				<th>Quantity</th>
        			</tr>					
        		</thead>
        		<tbody>{cart}</tbody>
        	</Table>
        	<Table striped bordered hover responsive>
        		<thead className="bg-dark text-danger">
	        		<tr>
	        			<th className = "d-flex justify-content-center">Total: &nbsp;<span>&#8369;</span>{cartTotal}</th>
	        		</tr>
	        		</thead>
        	</Table>
        	<Container fluid className = "text-center">
        		<Button className = "w-25" onClick = {() => { if (items.length <= 0){
        			Swal.fire({
        				title: "Cart is empty",
        				icon: "error",
        				text: "You don't have items on your cart, please browse our products to proceed in checkout."
        			})
        			}
        			else{setShowModal(true)}}}>Confirm</Button>
        	</Container>

        	<Modal show = {showModal} onHide = {closeModal}>
        		<Modal.Header closeButton>
	        		<Modal.Title>Confirm your address and payment method</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit = {onSubmit}>
				        <Form.Group controlId="address">
					        <Form.Label>SHIPPING ADDRESS:</Form.Label>
					        <Form.Control
					            size ="md" 
							    type="address" 
							    placeholder="Enter address" 
							    value = {address} 
							    onChange = {e=>setAddress(e.target.value)}	
							    required/>
				    </Form.Group>

				    <Form.Group controlId="paymentMethod" className = "mt-2">
				        <Form.Label>Payment Method:</Form.Label><br></br>
				  		<Form.Control
					            size ="md" 
							    placeholder="I.E Gcash, Cod, Debit Card" 
							    value = {paymentMethod} 
							    onChange = {e=>setPaymentMethod(e.target.value)}	
							    required/>
				    </Form.Group>
				    <div className = "text-center">
				    <Button className= "mt-4" variant="primary" type="submit" id="submitBtn" onClick={emptyCart}>
				      PLACE ORDER
				    </Button>
				    </div>
				    </Form>
				</Modal.Body>
        	</Modal>
    	</Container>
	)
}