import { Fragment, useState, useEffect } from 'react'
import AppNavbar from "../pages/AppNavbar";

export default function Cart({productId}) {
	console.log(productId)


    const [products, setProducts] = useState([]);
    useEffect(()=>{
    	const filter = (productId) => {
    		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/preview`)
    		.then(res => res.json())
    		.then(data => {
    			setProducts(data);
    			console.log(products)
    		})
    	}
    },[productId])

    return(
        <Fragment>
            {products}
        </Fragment>
    );
}