import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
	
	const data = {
		title: "AviloPC Computer store",
		content: "PC package, laptops and PC components and peripherals",
		destination: "/products",
		label: "Browse products"
	}

	return(
		<Fragment>
			<Banner data={data} />
			<Highlights />
		</Fragment>
	)
}