import { Fragment, useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ViewUsers (props) {
	const { usersData, fetchUsers } = props;
	const [newUsers, setNewUsers] = useState([]);


	useEffect(() => {

		const setAsAdminToggle = (userId, isAdmin) => {

			fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAsAdmin`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isAdmin: !isAdmin
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {
					fetchUsers();
					Swal.fire({
						title: "Success",
						icon: "success",
						text: "User successfully set as Admin/User"
					});

				} else {
					fetchUsers();
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const userArray = usersData.map(user => {

			return(
				<tr key={user._id}>
					<td>{user.firstname}</td>
					<td>{user.lastname}</td>
					<td>{user.email}</td>
					<td>{user.mobilenumber}</td>
					<td>
					{user.isAdmin
						? <span>Admin</span>
						: <span>User</span>
					}
					</td>
					<td className = "container-fluid d-flex justify-content-center">
					{user.isAdmin
						?
						<Button 
							variant="dark" 
							onClick={() => setAsAdminToggle(user._id, user.isAdmin)}>
							setAsUser
						</Button>
						:
						<Button 
							variant="danger"
							onClick={() => setAsAdminToggle(user._id, user.isAdmin)}
						>
							setAsAdmin
						</Button>
						}
					</td>
				</tr>
			)
		});
		setNewUsers(userArray);
	}, [usersData, fetchUsers]);

	return(
		<Fragment>
			<div className="mt-5 pt-4">
				<div className = "text-center">
				<h2>Admin Dashboard</h2>
				</div>
				<Button className = "m-3" variant="dark" onClick={fetchUsers}>Refresh users</Button>			
			</div>
			<Table striped bordered hover responsive className = "m-1">
				<thead className="bg-dark text-white">
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Mobile Number</th>
						<th>User Type</th>
						<th></th>
					</tr>					
				</thead>
				<tbody>
					{newUsers}
				</tbody>
			</Table>	
		</Fragment>
	)
}