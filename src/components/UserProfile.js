import { react, useState, useEffect } from 'react';
import { Container, Button, Table, Image, Modal, Form } from 'react-bootstrap';
import { useNavigate } from "react-router";
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function UserProfile (props){
	const [show, setShow] = useState(false);
	const openModal = () => setShow(true);
	const closeModal = () => setShow(false);
	const [openOrder, setOpenOrder] = useState(false);
	const openOrders = () => setOpenOrder(true);
	const closeOrders = () => setOpenOrder(false);
	const [fname, setFname] = useState('');
	const [lname, setLname] = useState('');
	const [email, setEmail] = useState('');
	const [phone, setPhone] = useState('');
	const [profile, setProfile] = useState('');
	const [orders, setOrders] = useState([]);
	const [filteredOrders, setFilteredOrders] = useState([]);
	const navigate = useNavigate();

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setProfile(data);
			setFname(data.firstname);
			setLname(data.lastname);
			setEmail(data.email);
			setPhone(data.mobilenumber);
		});
		fetch(`${process.env.REACT_APP_API_URL}/users/profile/myOrders`,{
    		method: 'GET',
    		headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
    	})
    	.then(res => res.json())
    	.then(data => {
    		setOrders(data.orders);
    	})
    	const newArr = orders.map((e)=>{
    		return e.items;	
    	})

    	function flatten(array) {
    	   return !Array.isArray(array) ? array : [].concat.apply([], array.map(flatten));
    	}
    	const object = (flatten(newArr));

    	const finalItems = object.map((e, index)=>{
    		return(
    			<tr key = {index}>
    				<td>{e.name}</td>
    				<td>{e.price}</td>
    				<td className = "text-center">{e.quantity}</td>
    				<td className = "text-danger">{e.itemTotal}</td>
    				<td>Pending</td>	
    			</tr>
    		)
    	})
    	setFilteredOrders(finalItems);
	},[show, openOrder])

	function editProfile (e){
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/profile/editprofile`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				firstname: fname,
				lastname: lname,
				email: email,
				mobilenumber: phone
			})
		})
		.catch(error=>{
			window.alert(error);
     		return;
		}).then(res => res.json()).then(data => {
			if(data === false){
				Swal.fire({
					title: "Error 404",
					icon: "error",
					text: "There seems to be a problem with the connection, please try again."
				})
			}
			else{
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Your profile has been updated."
				});
				navigate("/UserProfile");
				setShow(false);
			}
		})
	}

	return(
		<>
		<Container fluid className = "bg-dark text-white profile-card">
			<div className="profile row container d-flex justify-content-center">
				<div className="col-xl-6 col-md-12">
                    <div className="cardProfile user-card-full">
                        <div className="row m-l-0 m-r-0">
                            <div className="col-sm-4 bg-c-lite-green user-profile">
                                <div className="card-block text-center text-white">
                                    <div className="m-b-25">
                                        <Image src="https://img.icons8.com/bubbles/100/000000/user.png" className="bg-dark" alt="User-Profile-Image"/>
                                    </div>
                                    <h6 className="f-w-600">{profile.firstname}&nbsp;{profile.lastname}</h6>
                                    <p>Customer</p>
                                    <i className=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                                </div>
                            </div>
                            <div className="col-sm-8">
                                <div className="card-block">
                                    <h6 className="m-b-20 p-b-5 b-b-default f-w-600">Information:</h6>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <p className="m-b-10 f-w-600">Email</p>
                                            <h6 className="text-muted f-w-400">{profile.email}</h6>
                                        </div>
                                        <div className="col-sm-6">
                                            <p className="m-b-10 f-w-600">Phone</p>
                                            <h6 className="text-muted f-w-400">{profile.mobilenumber}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</Container>
		<div className = "d-flex justify-content-center mt-3">
            	<Button variant = "dark" onClick = {openModal}>Update Profile</Button>
            	<span>&nbsp;</span>
            	<Button variant = "dark" onClick = {openOrders}>View my orders</Button>
            </div>
        <Modal show={show} onHide={closeModal}>
        	<Form onSubmit={e => editProfile(e)}>
        		<Modal.Header closeButton>
        			<Modal.Title>Edit Profile</Modal.Title>
        		</Modal.Header>
        		<Modal.Body>
        			<Form.Group controlId="fname">
        				<Form.Label>First Name</Form.Label>
        				<Form.Control type="text" value={fname} onChange={e => setFname(e.target.value)} required/>
        			</Form.Group>	
        			<Form.Group controlId="productName">
        				<Form.Label>Last Name</Form.Label>
        				<Form.Control type="text" value={lname} onChange={e => setLname(e.target.value)} required/>
        			</Form.Group>
        			<Form.Group controlId="productDescription">
        				<Form.Label>Email</Form.Label>
        				<Form.Control type="text" value={email}  onChange={e => setEmail(e.target.value)} required/>
        			</Form.Group>
        			<Form.Group controlId="productPrice">
        				<Form.Label>Mobile Number</Form.Label>
        				<Form.Control type="number" value={phone}  onChange={e => setPhone(e.target.value)} required/>
        			</Form.Group>
        		</Modal.Body>
        		<Modal.Footer>
        			<Button variant="secondary" onClick={closeModal}>Close</Button>
        			<Button variant="success" type="submit">Submit Changes</Button>
        		</Modal.Footer>
        	</Form>
        </Modal>
        <Modal show={openOrder} onHide={closeOrders}>
				<Modal.Header closeButton>
					<Modal.Title>Your Orders</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Container fluid>
						<Table striped bordered hover responsive>
							<thead className="bg-dark text-white">
								<tr>
									<th>Name</th>
									<th>Price</th>
									<th>Quantity</th>
									<th>Item Total</th>
									<th>Status</th>
								</tr>					
							</thead>
							<tbody>{filteredOrders}</tbody>
						</Table>
					</Container>
				</Modal.Body>
			</Modal>
        </>
	)
}