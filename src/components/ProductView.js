import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { CartProvider, useCart } from "react-use-cart";


export default function ProductView(){

	const { user } = useContext(UserContext);
	const { productId } = useParams();
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [counter, setCounter] = useState(1);
	const [cart, setCart] = useState('');
	const {
		addItem,
	    isEmpty,
	    totalUniqueItems,
	    items,
	    updateItemQuantity,
	    removeItem,
	    totalItems
	  } = useCart();

	useEffect(()=>{
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}/preview`)
		.then(res => res.json())
		.then(data=>{
			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			const newCart = {
				id: data._id,
				name: data.productName,
				description: data.description,
				price: data.price
			};
			setCart(newCart);
		})
	},[productId]);

	function addToCart() {
		addItem(cart);
		if(cart){
			Swal.fire({
				title: "Success",
				icon: "success",
				text: "Item has added on your cart. You may adjust the quantity on your cart"
			})
		}
	}

	return(
		<Container fluid className = "userProductView">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card className = "product-details-card">
						<Card.Body className="text-center container-fluid">
							<img src = {`../images/${productId}.jpg`} alt = "product-img" className = "product-img img-fluid"/>
							<Card.Title>{name}</Card.Title>
							<br></br>
							<Card.Subtitle>Description:</Card.Subtitle>
							<br></br>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price: </Card.Subtitle>
							<Card.Text><span>&#8369;</span>{price}</Card.Text>
							<Row className = "mb-3">
								<Col>
									<Link to="/products">
									     <Button type = "button" variant = "primary">
									          Back to products page
									     </Button>
									 </Link>
								</Col>
								<Col>
									{ (user.id !== null)?
									<Button variant="primary" onClick={()=>addToCart()}>Add to cart</Button>
									:
									<Link className='btn btn-danger' to='/login'>Login to continue shopping</Link>
									}
								</Col>
								
							</Row>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}