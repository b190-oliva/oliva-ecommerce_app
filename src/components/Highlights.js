import { Container, Row, Col, Button, Card } from 'react-bootstrap';
import { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Highlights(){
	
	return(
		<Container fluid className = "featured-container text-center bg-dark">
			<Button variant = "light" className = "featuredButton"> Featured</Button>
			{/*<div className = "card-container">
		    	<div className="products-card" key={_id}>
		            <div className="card_img">
		                <img className = "img" src= {`./images/${_id}.jpg`} alt = "product-img" />
		            </div>
		            <div className="card_header">
		                <h4 className = "text-dark">{productName}</h4> 
		                <Link className="btn btn-dark" to ={`/products/${_id}`}>Details</Link>
		            </div>
		        </div>  
		    </div>*/}
			<Row>
				<Col sm = {12} md = {6}>
					<img
			          className="d-block h-100 img-fluid"
			          src="./images/featured.jpg"
			          alt="featured"
			        />
				</Col>
				<Col sm = {12} md = {6}>
					<img
			          className="d-block h-100 img-fluid"
			          src="./images/featured2.jpg"
			          alt="featured"
			        />
				</Col>
			</Row>
			<Link to="/products">
		    	     <Button type="button" variant = "light" className = "shopNowButton">
		    	          SHOP NOW!
		    	     </Button>
		    	 </Link>
				<Col className = "col-12 my-3">
					<img
			          className="d-block w-100 img-fluid"
			          src="./images/Banner2.jpg"
			          alt="featured"
			        />
			    </Col>
			<Row>
			    <Col className = "col-6 my-3">
					<img
			          className="d-block w-100 img-fluid"
			          src="./images/featured3.jpg"
			          alt="featured"
			        />
			    </Col>
			    <Col className = "col-6 my-3">
					<img
			          className="d-block w-100 img-fluid"
			          src="./images/featured4.jpg"
			          alt="featured"
			        />
			    	<Link to="/products">
			    	     <Button type="button" variant = "primary" className = "browseAllButton">
			    	          Browse All Products
			    	     </Button>
			    	 </Link>
			    </Col>
			</Row>
		</Container>
	)
}