import { Fragment } from 'react';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {   
	const {productName, _id} = productProp;

    return (
    	<Fragment>
	    	<div className = "card-container">
		    	<div className="products-card" key={_id}>
		            <div className="card_img">
		                <img className = "img" src= {`./images/${_id}.jpg`} alt = "product-img" />
		            </div>
		            <div className="card_header">
		                <h4 className = "text-dark">{productName}</h4> 
		                <Link className="btn btn-dark" to ={`/products/${_id}`}>Details</Link>
		            </div>
		        </div>  
		    </div>
		</Fragment>
	);
}
