import { Container } from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';

export default function Banner({data}){

	return(
		<Container fluid className = "pt-2 bg-dark">
			<Carousel variant = "dark">
		      <Carousel.Item interval={3000}>
		        <img
		          className="d-block w-100 img-fluid"
		          src="./images/banner.jpg"
		          alt="First slide"
		        />
		      </Carousel.Item>
		      <Carousel.Item interval={3000}>
		        <img
		          className="d-block w-100 img-fluid"
		          src="./images/featured5.jpg"
		          alt="./images/Banner4.jpg"
		        />
		      </Carousel.Item>
		      <Carousel.Item interval={3000}>
		        <img
		          className="d-block w-100 img-fluid"
		          src="./images/banner3.jpg"
		          alt="Third slide"
		        />
		      </Carousel.Item>
		    </Carousel>
		</Container>	
	)
}