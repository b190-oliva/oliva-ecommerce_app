import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	const { productData, fetchData } = props;
	const [productId, setProductId] = useState("");
	const [products, setProducts] = useState([]);
	const [newData, setNewData] = useState([]);
	const [newProducts, setNewProducts] = useState([]);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [counter, setCounter] = useState(0);
	const [productType, setProductType] = useState("");
	const [category, setCategory] = useState("");
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	const openEdit = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }/preview`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setProductType(data.productType);
			setCategory(data.category);
			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
		})
		setShowEdit(true);
	};

	const closeEdit = () => {

		setShowEdit(false);
		setCategory("");
		setProductType("");
		setName("");
		setDescription("");
		setPrice(0);

	};

	const addProduct = (e) => {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				category: category,
				productType: productType,
				productName: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {
				fetchData();
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course successfully added."					
				})
				setCategory("")
				setProductType("")
				setName("")
				setDescription("")
				setPrice(0)
				closeAdd();

			} else {
				fetchData();
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const editProduct = (e, productId) => {
		
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/products/${ productId }/update`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				category: category,
				productType: productType,
				productName: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course successfully updated."
				});

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}

	useEffect(() => {

		const archiveToggle = (productId, isActive) => {

			fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }/archive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully archived/unarchived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const productArr = productData.map(product => {
			console.log(product)
			return(
				<tr key={product._id}>
					<td>{product.productName}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
						
						{product.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td>
						<Button
							variant="dark"
							size="md"
							onClick={() => openEdit(product._id)}
						>
							Update
						</Button>
						
						{product.isActive
							?
							<Button 
								variant="danger" 
								size="md" 
								onClick={() => archiveToggle(product._id, product.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								variant="success"
								size="md"
								onClick={() => archiveToggle(product._id, product.isActive)}
							>
								Enable
							</Button>
						}
					</td>
				</tr>
			)
		});
		setProducts(productArr);
		setCounter(0);
	}, [productData, fetchData]);

	const filter = (category) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${category}/filter`,{
			method: 'GET',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => {
			setNewData(data);
			setCounter(1);
		})
	}

	useEffect(() => {

		const archiveToggle = (productId, isActive) => {

			fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }/archive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Course successfully archived/unarchived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}


		const productArr = newData.map(newProduct => {

			return(
				<tr key={newProduct._id}>
					<td>{newProduct.productName}</td>
					<td>{newProduct.description}</td>
					<td>{newProduct.price}</td>
					<td>
						
						{newProduct.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td>
						<Button
							variant="dark"
							size="md"
							onClick={() => openEdit(newProduct._id)}
						>
							Update
						</Button>
						
						{newProduct.isActive
							?
							<Button 
								variant="danger" 
								size="md" 
								onClick={() => archiveToggle(newProduct._id, newProduct.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								variant="success"
								size="md"
								onClick={() => archiveToggle(newProduct._id, newProduct.isActive)}
							>
								Enable
							</Button>
						}
					</td>
				</tr>
			)
		});
		setNewProducts(productArr);
	},[counter, newData])

	return(
		<Fragment>
			<div className="row mt-5 pt-3 my-4">
				<div className = "text-center">
				<h2>Admin Dashboard</h2>
				</div>
				<div className="col-6">
					<Button variant="dark" id = "filter-button" className = "ml-2" as = "select" onChange = {e => filter(e.target.value)}>
						<option>Select Category</option>
						<option value="PCcomponents">PC Components</option>
						<option value="Peripherals">Peripherals</option>
						<option value="PCpackageAndLaptop">PC Package/Laptop</option>
					</Button>			
				</div>
				<div className="col-6 d-flex justify-content-end">
					<Button id = "add-product-btn" variant="dark" onClick={openAdd}>Add Product</Button>
					<Button id = "refresh-btn" variant="dark" onClick={fetchData}>Refresh products</Button>			
				</div>			
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>					
				</thead>
				{counter === 0 ?
				<tbody>
						{products}
				</tbody>
				:
				<tbody>
						{newProducts}
				</tbody>
				}
			</Table>

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productCategory">
							<Form.Label>Category</Form.Label>
							<Form.Control as = "select" value = {category} onChange={e => setCategory(e.target.value)}>
							      <option>Select Category:</option>
							      <option value="PCcomponents">PC Components</option>
							      <option value="Peripherals">Peripherals</option>
							      <option value="PCpackageAndLaptops">PC Package/Laptop</option>
							</Form.Control>
						</Form.Group>
						<Form.Group controlId="productType">
							<Form.Label>Product Type</Form.Label>
							<Form.Control type="text" value={productType} onChange={e => setProductType(e.target.value)} required/>
						</Form.Group>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="productCategory">
							<Form.Label>Category</Form.Label>
							<Form.Control as = "select" value={category} onChange={e => setCategory(e.target.value)}>
							      <option value="PCcomponents">PC Components</option>
							      <option value="Peripherals">Peripherals</option>
							      <option value="PCpackageAndLaptop">PC package/Laptop</option>
							</Form.Control>
						</Form.Group>
						<Form.Group controlId="productType">
							<Form.Label>Product Type</Form.Label>
							<Form.Control type="text" value={productType} onChange={e => setProductType(e.target.value)} required/>
						</Form.Group>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Fragment>
	)
}

